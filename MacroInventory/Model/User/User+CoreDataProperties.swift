////
////  User+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//
//extension User {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
//        return NSFetchRequest<User>(entityName: "User")
//    }
//
//    @NSManaged public var name: String?
//    @NSManaged public var userToken: String?
//    @NSManaged public var userId: UUID?
//    @NSManaged public var transactionId: NSSet?
//    @NSManaged public var teamId: NSSet?
//    @NSManaged public var ownsTeam: NSSet?
//
//}
//
//// MARK: Generated accessors for transactionId
//extension User {
//
//    @objc(addTransactionIdObject:)
//    @NSManaged public func addToTransactionId(_ value: Transaction)
//
//    @objc(removeTransactionIdObject:)
//    @NSManaged public func removeFromTransactionId(_ value: Transaction)
//
//    @objc(addTransactionId:)
//    @NSManaged public func addToTransactionId(_ values: NSSet)
//
//    @objc(removeTransactionId:)
//    @NSManaged public func removeFromTransactionId(_ values: NSSet)
//
//}
//
//// MARK: Generated accessors for teamId
//extension User {
//
//    @objc(addTeamIdObject:)
//    @NSManaged public func addToTeamId(_ value: Team)
//
//    @objc(removeTeamIdObject:)
//    @NSManaged public func removeFromTeamId(_ value: Team)
//
//    @objc(addTeamId:)
//    @NSManaged public func addToTeamId(_ values: NSSet)
//
//    @objc(removeTeamId:)
//    @NSManaged public func removeFromTeamId(_ values: NSSet)
//
//}
//
//// MARK: Generated accessors for ownsTeam
//extension User {
//
//    @objc(addOwnsTeamObject:)
//    @NSManaged public func addToOwnsTeam(_ value: Team)
//
//    @objc(removeOwnsTeamObject:)
//    @NSManaged public func removeFromOwnsTeam(_ value: Team)
//
//    @objc(addOwnsTeam:)
//    @NSManaged public func addToOwnsTeam(_ values: NSSet)
//
//    @objc(removeOwnsTeam:)
//    @NSManaged public func removeFromOwnsTeam(_ values: NSSet)
//
//}
//
//extension User : Identifiable {
//}
