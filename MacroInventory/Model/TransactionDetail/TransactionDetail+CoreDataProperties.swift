////
////  TransactionDetail+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//
//extension TransactionDetail {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<TransactionDetail> {
//        return NSFetchRequest<TransactionDetail>(entityName: "TransactionDetail")
//    }
//
//    @NSManaged public var quantity: Double
//    @NSManaged public var transactionDetailId: UUID?
//    @NSManaged public var itemDetails: ItemDetails?
//    @NSManaged public var transactionId: Transaction?
//
//}
//
//extension TransactionDetail : Identifiable {
//}
