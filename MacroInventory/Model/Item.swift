import Foundation
struct Item {
    var itemName: String
    var description: String
    var price: Double
    var stock: Double
    var sku: String
    var image: Data
    var teamId: String
}

