//
//  UserModel.swift
//  MacroInventory
//
//  Created by bevan christian on 13/10/21.
//

import Foundation

struct UserModel: Codable {
    var name: String
    var email: String
    var userId: String
    
    init(name:String,email:String,userId:String) {
        self.name = name
        self.email = email
        self.userId = userId
    }
}
