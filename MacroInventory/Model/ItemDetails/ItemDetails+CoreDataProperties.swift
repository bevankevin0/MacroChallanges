////
////  ItemDetails+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//
//extension ItemDetails {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemDetails> {
//        return NSFetchRequest<ItemDetails>(entityName: "ItemDetails")
//    }
//
//    @NSManaged public var price: Double
//    @NSManaged public var sku: String?
//    @NSManaged public var stock: Double
//    @NSManaged public var varian: String?
//    @NSManaged public var itemDetailsId: UUID?
//    @NSManaged public var itemId: Item?
//    @NSManaged public var transactionDetail: NSSet?
//
//}
//
//// MARK: Generated accessors for transactionDetail
//extension ItemDetails {
//
//    @objc(addTransactionDetailObject:)
//    @NSManaged public func addToTransactionDetail(_ value: TransactionDetail)
//
//    @objc(removeTransactionDetailObject:)
//    @NSManaged public func removeFromTransactionDetail(_ value: TransactionDetail)
//
//    @objc(addTransactionDetail:)
//    @NSManaged public func addToTransactionDetail(_ values: NSSet)
//
//    @objc(removeTransactionDetail:)
//    @NSManaged public func removeFromTransactionDetail(_ values: NSSet)
//
//}
//
//extension ItemDetails : Identifiable {
//}
