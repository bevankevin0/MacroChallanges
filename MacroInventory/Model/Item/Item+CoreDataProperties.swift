////
////  Item+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//extension Item {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
//        return NSFetchRequest<Item>(entityName: "Item")
//    }
//
//    @NSManaged public var image: Data?
//    @NSManaged public var itemDescription: String?
//    @NSManaged public var itemName: String?
//    @NSManaged public var itemId: UUID?
//    @NSManaged public var teamId: Team?
//    @NSManaged public var variants: NSSet?
//
//}
//
//// MARK: Generated accessors for variants
//extension Item {
//
//    @objc(addVariantsObject:)
//    @NSManaged public func addToVariants(_ value: ItemDetails)
//
//    @objc(removeVariantsObject:)
//    @NSManaged public func removeFromVariants(_ value: ItemDetails)
//
//    @objc(addVariants:)
//    @NSManaged public func addToVariants(_ values: NSSet)
//
//    @objc(removeVariants:)
//    @NSManaged public func removeFromVariants(_ values: NSSet)
//
//}
//
//extension Item : Identifiable {
//}
