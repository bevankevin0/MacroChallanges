////
////  Transaction+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//
//extension Transaction {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transaction> {
//        return NSFetchRequest<Transaction>(entityName: "Transaction")
//    }
//
//    @NSManaged public var totalHarga: Double
//    @NSManaged public var transactionDate: Date?
//    @NSManaged public var transactionId: UUID?
//    @NSManaged public var userId: User?
//    @NSManaged public var transactionDetail: NSSet?
//
//}
//
//// MARK: Generated accessors for transactionDetail
//extension Transaction {
//
//    @objc(addTransactionDetailObject:)
//    @NSManaged public func addToTransactionDetail(_ value: TransactionDetail)
//
//    @objc(removeTransactionDetailObject:)
//    @NSManaged public func removeFromTransactionDetail(_ value: TransactionDetail)
//
//    @objc(addTransactionDetail:)
//    @NSManaged public func addToTransactionDetail(_ values: NSSet)
//
//    @objc(removeTransactionDetail:)
//    @NSManaged public func removeFromTransactionDetail(_ values: NSSet)
//
//}
//
//extension Transaction : Identifiable {
//}
