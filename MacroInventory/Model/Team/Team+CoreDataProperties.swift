////
////  Team+CoreDataProperties.swift
////  MacroInventory
////
////  Created by Priscilla Vanny Amelia on 04/10/21.
////
////
//
//import Foundation
//import CoreData
//
//extension Team {
//
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<Team> {
//        return NSFetchRequest<Team>(entityName: "Team")
//    }
//
//    @NSManaged public var teamName: String?
//    @NSManaged public var teamId: UUID?
//    @NSManaged public var ownsItems: NSSet?
//    @NSManaged public var userId: NSSet?
//    @NSManaged public var ownedBy: User?
//
//}
//
//// MARK: Generated accessors for ownsItems
//extension Team {
//
//    @objc(addOwnsItemsObject:)
//    @NSManaged public func addToOwnsItems(_ value: Item)
//
//    @objc(removeOwnsItemsObject:)
//    @NSManaged public func removeFromOwnsItems(_ value: Item)
//
//    @objc(addOwnsItems:)
//    @NSManaged public func addToOwnsItems(_ values: NSSet)
//
//    @objc(removeOwnsItems:)
//    @NSManaged public func removeFromOwnsItems(_ values: NSSet)
//
//}
//
//// MARK: Generated accessors for userId
//extension Team {
//
//    @objc(addUserIdObject:)
//    @NSManaged public func addToUserId(_ value: User)
//
//    @objc(removeUserIdObject:)
//    @NSManaged public func removeFromUserId(_ value: User)
//
//    @objc(addUserId:)
//    @NSManaged public func addToUserId(_ values: NSSet)
//
//    @objc(removeUserId:)
//    @NSManaged public func removeFromUserId(_ values: NSSet)
//
//}
//
//extension Team : Identifiable {
//}
