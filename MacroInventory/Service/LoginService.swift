//
//  LoginService.swift
//  MacroInventory
//
//  Created by bevan christian on 13/10/21.
//

import Foundation
import AuthenticationServices
import Combine

class LoginService: NSObject, ObservableObject, ASAuthorizationControllerDelegate {
    var isSuccessNewUser = PassthroughSubject<Bool, Never>()
    var isSuccessOldUser = PassthroughSubject<Bool, Never>()
    func signInTapped() {
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        performSignIn(using: [request])
    }
    private func performSignIn(using requests: [ASAuthorizationRequest]) {
        let controller = ASAuthorizationController(authorizationRequests: requests)
        controller.delegate = self
        controller.performRequests()
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIdCredential as ASAuthorizationAppleIDCredential:
            if appleIdCredential.fullName != nil && appleIdCredential.email != nil {
                // kalo belum pernah login ya create team / join team
                let userIdentifier = appleIdCredential.user
                let fullName = appleIdCredential.fullName
                let email = appleIdCredential.email ?? ""
                let name = (fullName?.givenName ?? "") + (" ") + (fullName?.familyName ?? "")
                let userModel = UserModel(name: name, email: email, userId: userIdentifier)
                let encodeData = try? JSONEncoder().encode(userModel)
                UserDefaults.standard.set(encodeData, forKey: Constant.userLoginProfile.stringValue)
                print("login lama")
                isSuccessNewUser.send(true)
            } else {
                // sudah pernah login dilempar ke homepage langsung dan di get team codenya apa?
                guard let appleUserData = UserDefaults.standard.data(forKey: Constant.userLoginProfile.stringValue),
                      let appleUser = try? JSONDecoder().decode(UserModel.self, from: appleUserData)
                else { return }
                isSuccessOldUser.send(true)
            }
        default:
            break
        }
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
}
