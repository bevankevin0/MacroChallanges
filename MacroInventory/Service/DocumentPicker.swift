//
//  DocumentPicker.swift
//  MacroInventory
//
//  Created by Priscilla Vanny Amelia on 12/10/21.
//

import Foundation
import SwiftUI
import MobileCoreServices

struct DocumentPicker: UIViewControllerRepresentable {
    @Binding var filePath: String

    func makeCoordinator() -> DocumentPickerCoordinator {
        return DocumentPickerCoordinator(filePath: $filePath)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<DocumentPicker>) -> UIDocumentPickerViewController {
        let controller: UIDocumentPickerViewController

        if #available(iOS 14, *) {
            controller = UIDocumentPickerViewController(forOpeningContentTypes: [.text], asCopy: false)
        } else {
            controller = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText)], in: .import)
        }
        return controller
    }

    func updateUIViewController(_ uiViewController: UIDocumentPickerViewController, context: UIViewControllerRepresentableContext<DocumentPicker>) {}
}

class DocumentPickerCoordinator: NSObject, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    @Binding var filePath: String

    init(filePath: Binding<String>) {
        _filePath = filePath
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt urls: [URL]) {
        filePath = urls[0].absoluteString
        print(filePath)
    }
}
