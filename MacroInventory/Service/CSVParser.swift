//
//  CSVParser.swift
//  MacroInventory
//
//  Created by Priscilla Vanny Amelia on 07/10/21.
//

import Foundation
import CSV

class CSVParser {
    let fileName: String
    let fileType: String
    let path: String
    let stream: InputStream

    init(fileName: String, fileType: String, path: String, stream: InputStream, csv: CSVReader) {
        self.fileName = fileName
        self.fileType = fileType
        self.path = Bundle.main.path(forResource: self.fileName, ofType: self.fileType) ?? ""
        self.stream = InputStream(fileAtPath: self.path) ?? InputStream()
        
    }

    func getCSVHeader() -> [String] {
        let csv = try? CSVReader(stream: self.stream, hasHeaderRow: true)
        let headerRow = csv!.headerRow ?? []

        return headerRow
    }
    
    func getCSVColumnValues(colName: String) -> [String] {
        let csv = try? CSVReader(stream: self.stream, hasHeaderRow: true)
        var column: [String] = []
        
        while csv?.next() != nil {
            let content = csv![colName]!
            if !content.trimmingCharacters(in: .whitespaces).isEmpty {
                column.append(content)
            }
        }
        
        return column
    }
}
