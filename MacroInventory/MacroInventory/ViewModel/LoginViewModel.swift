//
//  LoginViewModel.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import Foundation
import Combine


class LoginViewModel: ObservableObject {
    @Published var isSuccessNewUser = false
    @Published var isSuccessOldUser = false
    @Published var joinCompanyCodeView = false
    var persistenceControler = PersistenceController.shared
    var loginService = LoginService()
    var subscription = Set<AnyCancellable>()
    var counter = 0
    
    init() {
        subscribingLoginService()
//        persistenceControler.debug()
    }
    
    private func subscribingLoginService() {
        loginService.isSuccessNewUser
            .sink { [self] hasil in
                print("sukses \(hasil)")
                isSuccessNewUser = hasil
            }.store(in: &subscription)
        
        loginService.isSuccessOldUser
            .sink { [self] hasil in
                print("sukses old user\(hasil)")
                isSuccessOldUser = hasil
            }.store(in: &subscription)
    }
    
    func signInTapped() {
        loginService.signInTapped()
    }
    
    
}
