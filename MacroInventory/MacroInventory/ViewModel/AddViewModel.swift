//
//  AddViewModel.swift
//  MacroInventory
//
//  Created by bevan christian on 04/10/21.
//

import Foundation
import Combine
import SwiftUI

class AddViewModel: ObservableObject {
    @Published var itemsContentPicOrVid = UIImage(named: "Nature")!
    @Published var isInvalid = true
    @Published var isInvalid1 = true
    @Published var isInvalid2 = true
    @Published var itemName = ""
    @Published var skuNumber = ""
    @Published var descriptonText = ""
    @Published var price = "0"
    @Published var varians = ""
    @Published var stockSelection = 0
    var subscription = Set<AnyCancellable>()
    
    init() {
        validation()
    }
    func append(item: UIImage) {
        itemsContentPicOrVid = item
        print("photo \(itemsContentPicOrVid)")
    }
    
    
    func insertItem() {
        
    }
}

extension AddViewModel {
    private func validation() {
        $itemName.combineLatest($skuNumber, $descriptonText, $price).sink {[self] name, sku, descript, price in
            if name.count > 5 && sku.count > 5 && descript.count > 5 && Int(price) ?? -1 > 0 {
                isInvalid1 = false
            } else {
                isInvalid1 = true
            }
        }.store(in: &subscription)
        $varians.combineLatest($itemsContentPicOrVid, $stockSelection).sink { [self] varian, content, stock in
            if !varian.isEmpty && content != UIImage(named: "Nature") && stock > 0 {
                isInvalid2 = false
            } else {
                isInvalid2 = true
            }
            $isInvalid1.combineLatest($isInvalid2).sink { atas, bawah in
                if atas == false && bawah == false {
                    isInvalid = false
                } else {
                    isInvalid = true
                }
            }.store(in: &subscription)
        }.store(in: &subscription)
    }
}
