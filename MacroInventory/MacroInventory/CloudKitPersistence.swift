//
//  CloudKitPersistence.swift
//  MacroInventory
//
//  Created by Fransiscus Verrol Yaurentius on 14/10/21.
//

import Foundation
import CloudKit
import CoreMedia
import SwiftUI

class CloudKitPersistenceController {
    static let shared = CloudKitPersistenceController()
    let database = CKContainer(identifier: "iCloud.MacroInventory").publicCloudDatabase
    func getItem() -> [Item] {
        let query = CKQuery(recordType: "item", predicate: NSPredicate(value: true))
        let operation = CKQueryOperation(query: query)
        operation.recordMatchedBlock = { id, item in
            switch item {
            case.success(let data):
                print(data)
            case.failure(let err):
                print(err)
            }
        }
        database.add(operation)
        return []
    }
    func addItem() {
        
    }
}
