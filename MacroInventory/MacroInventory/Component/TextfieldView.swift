//
//  TextfieldView.swift
//  MacroInventory
//
//  Created by bevan christian on 04/10/21.
//

import SwiftUI

struct TextfieldView: View {
    @Binding var variabelSimpan: String
    var labelTextfield: String
    var body: some View {
            Section {
                TextField("\(labelTextfield)", text: $variabelSimpan)
            } header: {
                Text("\(labelTextfield)")
            }
    }
}

struct TextfieldView_Previews: PreviewProvider {
    static var previews: some View {
        TextfieldView(variabelSimpan: .constant("hello"), labelTextfield: "halo")
    }
}
