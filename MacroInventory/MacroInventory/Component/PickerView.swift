//
//  PickerView.swift
//  MacroInventory
//
//  Created by bevan christian on 04/10/21.
//

import SwiftUI

struct PickerView: View {
    @Binding var stockSelection: Int
    var body: some View {
        Section {
            Picker(selection: $stockSelection) {
                ForEach(1..<10) { num in
                    Text("\(num)")
                }
            } label: {
                Text("Stok")
            }
        } header: {
            Text("Stok")
        }
    }
}
