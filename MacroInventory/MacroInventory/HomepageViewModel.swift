//
//  HomepageViewModel.swift
//  MacroInventory
//
//  Created by Kezia Yovita Chandra on 05/10/21.
//
import Foundation
import CloudKit

class HomePageViewModel: ObservableObject {
    @Published var itemList: [Item] = [Item]()
    
    let Database = CKContainer(identifier: "iCloud.Macro_Database").publicCloudDatabase
    
}

extension HomePageViewModel {
    func getItemData() {
//        itemList = PersistenceController.shared.fetchItems()
    }
}
