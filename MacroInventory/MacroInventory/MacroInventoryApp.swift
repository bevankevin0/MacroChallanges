//
//  MacroInventoryApp.swift
//  MacroInventory
//
//  Created by bevan christian on 01/10/21.
//

import SwiftUI

@main
struct MacroInventoryApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            if UserDefaults.standard.data(forKey: Constant.userLoginProfile.stringValue) == nil {
                LoginView()
                    .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                    
            } else {
                HomepageView()
                    .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
            }
                           
        }
    }
}


extension UIApplication {
    func addTapGestureRecognizer() {
        guard let window = windows.first else { return }
        let tapGesture = UITapGestureRecognizer(target: window, action: #selector(UIView.endEditing))
        tapGesture.requiresExclusiveTouchType = false
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        window.addGestureRecognizer(tapGesture)
    }
}

extension UIApplication: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true // set to `false` if you don't want to detect tap during other gestures
    }
}
