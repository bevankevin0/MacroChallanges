//
//  Helper.swift
//  MacroInventory
//
//  Created by bevan christian on 04/10/21.
//

import Foundation
import SwiftUI

struct Theme {
    var navbarLight = Color("navbarLight")
    var primaryColor = Color("primaryColor")
    var keyboardFrontNormal = Color("KeyboardFrontNormal")
    var keyboardBackground = Color("KeyboardBackground")
    var keyboardFrontHelper = Color("KeyboardFrontHelper")
    

}

extension Color {
    static let themes = Theme()
}
