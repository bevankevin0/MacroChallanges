//
//  Persistence.swift
//  MacroInventory
//
//  Created by bevan christian on 01/10/21.
//

import CoreData

class PersistenceController {
    static let shared = PersistenceController()
//    var container: NSPersistentCloudKitContainer
//    init(inMemory: Bool = false) {
//        container = NSPersistentCloudKitContainer(name: "MacroInventory")
//        if inMemory {
//            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
//        }
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//    }
//    func createContext() -> NSManagedObjectContext {
//        let taskContext = container.viewContext
//        return taskContext
//    }
//    func delete(item: Item) {
//        container.viewContext.delete(item)
//        save()
//    }
//    func save() {
//        do {
//            try container.viewContext.save()
//        } catch let error {
//            print("Error saving. \(error)")
//        }
//    }
//    func fetchItems() -> [Item] {
//        let request = NSFetchRequest<Item>(entityName: "Item")
//        do {
//            return try container.viewContext.fetch(request)
//        } catch let error {
//            print("Error fetching. \(error)")
//        }
//        return []
//    }
//    @objc func processingUpdateFromCloudKit() {
//        print("Test")
//    }
//    private func createPersitenceContainer() {
//        guard let description  = container.persistentStoreDescriptions.first else { fatalError()}
//        // akan memberikan notifikasi setiap ada perubahan di cloudkit
//        description.setOption(true as NSObject, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
//        container.viewContext.automaticallyMergesChangesFromParent = true
//        container.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
//        container.loadPersistentStores { description, error in
//            if error != nil {
//                fatalError()
//            }
//        }
//        NotificationCenter.default.addObserver(self, selector: #selector(processingUpdateFromCloudKit), name: .NSPersistentStoreRemoteChange, object: nil)
//    }
}

// bevan
//mport CoreData
//import CloudKit
//
//
//class PersistenceController {
//    static let shared = PersistenceController()
//    var container: NSPersistentCloudKitContainer
//    init() {
//        container = NSPersistentCloudKitContainer(name: "MacroInventory")
//        createPersitenceContainer()
//    var container = NSPersistentContainer(name: "MacroInventory")
//    var counter = 0
//    init(inMemory: Bool = false) {
////        container = NSPersistentContainer(name: "MacroInventory")
////        if inMemory {
////            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
////        }
////        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
////            if let error = error as NSError? {
////                fatalError("Unresolved error \(error), \(error.userInfo)")
////            }
////        })
//        self.createPersitenceContainer()
//    }
//
//    func createContext() -> NSManagedObjectContext {
//        let taskContext = container.viewContext
//        return taskContext
//    private func createPersitenceContainer() {
//        guard let description  = container.persistentStoreDescriptions.first else { fatalError()}
//        // akan memberikan notifikasi setiap ada perubahan di cloudkit
//        description.setOption(true as NSObject, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
//        description.cloudKitContainerOptions?.databaseScope = .public
//        container.viewContext.automaticallyMergesChangesFromParent = true
//        container.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
////        description.setOption(true as NSObject, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
////        container.viewContext.automaticallyMergesChangesFromParent = true
////        container.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
//        container.loadPersistentStores { description, error in
//            if error != nil {
//                fatalError()
//            }
//        }
//        NotificationCenter.default.addObserver(self, selector: #selector(processingUpdateFromCloudKit), name: .NSPersistentStoreRemoteChange, object: nil)
////        NotificationCenter.default.addObserver(self, selector: #selector(processingUpdateFromCloudKit), name: .NSPersistentStoreRemoteChange, object: nil)
//    }
//
//
//    // MARK: FUNCTION CREATE COMPANY
//
//    func saveCompanyProfile(name: String, companyName: String,teamCode:String) {
//        let userData = UserDefaultData.shared.getUserData()
//        let taskContext = container.viewContext
//        taskContext.perform {
//            let user = User(context: taskContext)
//            user.userId = UUID()
//            user.name = name
//            user.email = userData?.email
//            user.isOwner = 1
//            user.userToken = userData?.userId
//            let team = Team(context: taskContext)
//            team.teamId = UUID()
//            team.teamName = companyName
//            team.ownedBy = user
//            team.teamCode = teamCode
//            team.addToUserId(user)
//        }
//        do {
//            try taskContext.save()
//        } catch let error {
//            print("Error saving. \(error)")
//        }
//    }
//
//    func checkCompanyCodeIsExist(teamCode:String,completion: @escaping(String)-> Void){
//        var result = [Team]()
//        let taskContext = container.viewContext
//        let teamCodeTampung = teamCode
//
//        taskContext.perform { [self] in
//            let request: NSFetchRequest<Team> = Team.fetchRequest()
//            let companyCodePredicate = NSPredicate(format: "\(#keyPath(Team.teamCode)) == %@",teamCode)
//            do {
//                result = try taskContext.fetch(request)
//            } catch let error {
//                print(error.localizedDescription)
//            }
//        }
//        // jika ga ada maka aman
//        if result.isEmpty {
//            completion(teamCodeTampung)
//        } else {
//            // jika ada recursive sampe ga ada teamcode
//            checkCompanyCodeIsExist(teamCode: String.random()) { teamcode in
//                completion(teamCode)
//            }
//        }
//    }
//
//
//
//    // MARK: JOIN COMPANY FUNCTION
//
//
//    func checkCompanyCodeValid(companyCode: String, completion: @escaping(Bool) -> Void) {
//        var result = [Team]()
//        let taskContext = container.viewContext
//        taskContext.perform {
//            let request: NSFetchRequest<Team> = Team.fetchRequest()
//            let companyCodePredicate = NSPredicate(format: "\(#keyPath(Team.teamCode)) == %@", companyCode)
//            do {
//                result = try taskContext.fetch(request)
//            } catch let error {
//                print(error.localizedDescription)
//            }
//        }
//        // jika ga ada maka code tidak valid
//        if result.isEmpty {
//            completion(false)
//        } else {
//        // jika ada maka valid
//            completion(true)
//        }
//    }
//
//
//    func getTeamByCode(companyCode: String,completion:@escaping(Result<[Team],CoreDataError>) -> Void) {
//        var result = [Team]()
//        let taskContext = container.viewContext
//        taskContext.perform {
//            let request: NSFetchRequest<Team> = Team.fetchRequest()
//            let companyCodePredicate = NSPredicate(format: "\(#keyPath(Team.teamCode)) == %@", companyCode)
//            do {
//                result = try taskContext.fetch(request)
//                completion(.success(result))
//            } catch let error {
//                print(error.localizedDescription)
//                completion(.failure(CoreDataError.coredataerror))
//            }
//        }
//    }
//
//    func saveEmployee(name:String,companyCode:String) {
//        getTeamByCode(companyCode: companyCode) { [self] result in
//            switch result {
//            case .success(let tim):
//                let userData = UserDefaultData.shared.getUserData()
//                let taskContext = container.viewContext
//                taskContext.perform {
//                    let user = User(context: taskContext)
//                    user.userId = UUID()
//                    user.name = name
//                    user.email = userData?.email
//                    user.isOwner = 0
//                    user.userToken = userData?.userId
//                    user.addToTeamId(tim.first!)
//                }
//                do {
//                    try taskContext.save()
//                } catch let error {
//                    print("Error saving. \(error)")
//                }
//            case .failure(let err):
//                print(err.localizedDescription)
//            }
//        }
//    }
//
//
//    // debug
//    func debug() {
//        var result = [Team]()
//        let taskContext = container.viewContext
//        taskContext.perform {
//            let request: NSFetchRequest<Team> = Team.fetchRequest()
//            do {
//                result = try taskContext.fetch(request)
//                print("result team ada apa aja \(result.count)")
//                print("result team ada apa aja \(result)")
//
//            } catch let error {
//                print(error.localizedDescription)
//                print("tim error \(error.localizedDescription)")
//            }
//        }
//    }
//
//
//
//
//}
