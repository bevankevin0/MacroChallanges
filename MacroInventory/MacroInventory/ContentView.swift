//
//  ContentView.swift
//  MacroInventory
//
//  Created by bevan christian on 01/10/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
    var body: some View {
        TabView {
            HomepageView().tabItem {
                Image(systemName: "shippingbox.fill")
                Text("Stock")
            }
            HistoryView().tabItem {
                Image(systemName: "filemenu.and.selection")
                Text("History")
            }
            AccountView().tabItem {
                Image(systemName: "person.fill")
                Text("Account")
            }
            ImportCSVPickView().tabItem{
                Image(systemName: "star")
                Text("Pick CSV")
            }
        }
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
