//
//  tes.swift
//  MacroInventory
//
//  Created by bevan christian on 11/10/21.
//

import SwiftUI

struct Tes: View {
    var body: some View {
        VStack(spacing:0) {
            
            Button {
                print("hell")
            } label: {
                ZStack {
                    Text("Hello, World!")
                        .frame(width: 50, height: 50)
                        .background(Color.red)
                    
                    Rectangle()
                        .frame(width: 50, height: 50)
                        .padding(5)
                        .background(Color.blue)
                        .opacity(0.2)
                        .layoutPriority(-1)
                }
            }
            Button {
                print("hell")
            } label: {
                ZStack {
                    Text("Hello, World!")
                        .frame(width: 50, height: 50)
                        .background(Color.red)

                    Rectangle()
                        .frame(width: 120, height: 80)
                        .opacity(0.3)
                }
            }
            Button {
                print("hell")
            } label: {
                ZStack {
                    Text("Hello, World!")
                        .frame(width: 50, height: 50)
                        .background(Color.red)

                    Rectangle()
                        .frame(width: 120, height: 80)
                        .opacity(0)
                }
            }
        }
    }
}

struct Tes_Previews: PreviewProvider {
    static var previews: some View {
        Tes()
    }
}
