//
//  AddItemView.swift
//  MacroInventory
//
//  Created by bevan christian on 04/10/21.
//

import SwiftUI

struct AddItemView: View {
    @Environment(\.colorScheme) var colorScheme
    @State var imagePresented = false
    @ObservedObject var addViewModel = AddViewModel()
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .leading) {
                    Form {
                        Image(uiImage: addViewModel.itemsContentPicOrVid)
                            .resizable()
                            .scaledToFill()
                            .clipped()
                            .frame(height: geo.size.height * 0.3, alignment: .center)
                            .onTapGesture {
                                imagePresented = true
                            }
                        Group {
                            TextfieldView(variabelSimpan: $addViewModel.itemName, labelTextfield: "Name")
                            TextfieldView(variabelSimpan: $addViewModel.skuNumber, labelTextfield: "SKU")
                            TextfieldView(variabelSimpan: $addViewModel.descriptonText, labelTextfield: "Description")
                            PickerView(stockSelection: $addViewModel.stockSelection)
                        }
                        Group {
                            TextfieldView(variabelSimpan: $addViewModel.price, labelTextfield: "Price")
                                .keyboardType(.numberPad)
                            TextfieldView(variabelSimpan: $addViewModel.varians, labelTextfield: "Varians")
                        }
                    }
                }.sheet(isPresented: $imagePresented) {
                    print("dismiss")
                } content: {
                    PHPickerView(didFinishPicking: { _ in
                        imagePresented = false
                    }, mediaItems: addViewModel)
                }.navigationBarItems(
                    trailing:
                        Button(action: {
                            addViewModel.insertItem()
                        }, label: {
                            Text("Add")
                                .disabled(addViewModel.isInvalid)
                                .foregroundColor(addViewModel.isInvalid ? Color.gray : Color.blue)
                        })
                )
            }
            .navigationBarTitle("Add Item", displayMode: .inline)
        }
    }
}

struct AddItemView_Previews: PreviewProvider {
    static var previews: some View {
        AddItemView()
    }
}
