//
//  PHPickerView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 01/09/21.
//

import Foundation
import PhotosUI
import SwiftUI

struct PHPickerView: UIViewControllerRepresentable {
    var didFinishPicking: (_ didSelectItems: Bool) -> Void
    typealias UIViewControllerType = PHPickerViewController
    @ObservedObject var mediaItems: AddViewModel
    func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .any(of: [.images])
        config.selectionLimit = 1
        config.preferredAssetRepresentationMode = .current
        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        return controller
    }
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
    }
    class Coordinator: PHPickerViewControllerDelegate {
        var photoPicker: PHPickerView
        init(with photoPicker: PHPickerView) {
            self.photoPicker = photoPicker
        }
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            photoPicker.didFinishPicking(!results.isEmpty)
            guard !results.isEmpty else {
                return
            }
            for result in results {
                let itemProvider = result.itemProvider
                guard let typeIdentfier = itemProvider.registeredTypeIdentifiers.first,
                      let uType = UTType(typeIdentfier)
                else { continue }
                if uType.conforms(to: .image) {
                    self.getPhoto(from: itemProvider, isLivePhoto: false)
                }
            }
        }
        private func getPhoto(from itemProvider: NSItemProvider, isLivePhoto: Bool) {
            let objectType: NSItemProviderReading.Type = !isLivePhoto ? UIImage.self : PHLivePhoto.self
            if itemProvider.canLoadObject(ofClass: objectType) {
                itemProvider.loadObject(ofClass: objectType) { object, error in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    if !isLivePhoto {
                        if let image = object as? UIImage {
                            DispatchQueue.main.async {
                               self.photoPicker.mediaItems.append(item: image)
                            }
                        }
                    }
                }
            }
        }
    }
}
