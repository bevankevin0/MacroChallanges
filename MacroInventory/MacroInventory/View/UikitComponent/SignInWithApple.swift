//
//  SignInWithApple.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import Foundation
import SwiftUI
import AuthenticationServices


final class SignInWithApple: UIViewRepresentable {
    func makeUIView(context: Context) -> some ASAuthorizationAppleIDButton {
        return ASAuthorizationAppleIDButton()
    }
    
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}
