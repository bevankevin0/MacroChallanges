//
//  StockInView.swift
//  MacroInventory
//
//  Created by Fransiscus Verrol Yaurentius on 07/10/21.
//

import SwiftUI

struct StockInView: View {
    var body: some View {
        ScrollView {
            ForEach(0 ..< 3) { i in
                HStack {
                    VStack(alignment: .leading) {
                        Text("#12345").font(.caption2).foregroundColor(.gray)
                        Text("The Goddness Check Paletee - Beige - 20 ml").font(.subheadline).bold()
                        Text("Bevan - Sales").font(.caption).padding(5).overlay(
                            Rectangle().stroke(Color.black, lineWidth: 1)
                        )
                    }
                    VStack(spacing: 5) {
                        Text("In").font(.caption).bold().foregroundColor(.gray)
                        Text("+ 50").font(.title3).bold()
                    }
                }.padding()
                Divider()
            }
        }
    }
}

struct StockInView_Previews: PreviewProvider {
    static var previews: some View {
        StockInView()
    }
}
