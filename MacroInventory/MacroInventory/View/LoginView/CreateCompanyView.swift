//
//  CreateCompanyView.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import SwiftUI
import Combine

struct CreateCompanyView: View {
    @ObservedObject var loginViewModel = CreateCompanyViewModel()
    @State var companyName = ""
    @State var ownerName = ""
    @State var joinCompanyCodeView = false
    let subscription = Set<AnyCancellable>()
    var roleList = ["Owner", "Employee"]
    var body: some View {
        VStack(alignment: .center, spacing: 30) {
            VStack(alignment:.center) {
                HStack {
                    Spacer()
                    Text("Create Company Profile")
                        .font(.title)
                    Spacer()
                }
            }
            VStack(alignment: .leading) {
                Text("Name")
                TextField("Owner Name", text: $ownerName, prompt: Text("Owner Name"))
                    .padding()
                    .border(.gray, width: 1)
            }
            VStack(alignment: .leading) {
                Text("Company Name")
                TextField("Company Name", text: $companyName, prompt: Text("Company Name"))
                    .padding()
                    .border(.gray, width: 1)
            }
            VStack(alignment: .center) {
                NavigationLink(isActive: $joinCompanyCodeView) {
                    CompanyCodeView(companyCode: loginViewModel.teamCode)
                } label: {
                    Button {
//                        loginViewModel.createCompanyCode(companyName: companyName, ownerName: ownerName)
                    } label: {
                        Text("Join")
                            .frame(width: 300, height: 40)
                            .background(.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                }
            }
         
        }.padding()
            .onChange(of: loginViewModel.selesaiCreateCompany) { newValue in
                print("satu")
                joinCompanyCodeView = newValue
            }
            
    }
}

struct CreateCompanyView_Previews: PreviewProvider {
    static var previews: some View {
        CreateCompanyView()
    }
}
