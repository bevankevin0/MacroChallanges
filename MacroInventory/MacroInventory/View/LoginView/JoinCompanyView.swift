//
//  JoinCompanyView.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import SwiftUI
import Combine

struct JoinCompanyView: View {
    @ObservedObject var loginViewModel = CreateCompanyViewModel()
    @State var employeeName = ""
    @State var companyCode = ""
    @State var codeInvalid = false
    @State var goToHomepage = false
    var body: some View {
        VStack(spacing:20) {
            Spacer()
            VStack(alignment:.center) {
                HStack {
                    Spacer()
                    Text("Join Company")
                        .font(.title)
                    Spacer()
                }
            }
            VStack(alignment: .leading) {
                Text("Name")
                TextField("Owner Name", text: $employeeName, prompt: Text("Owner Name"))
                    .padding()
                    .border(.gray, width: 1)
            }
            VStack(alignment:.leading) {
                Text("Company Code")
                TextField("Company Name", text: $companyCode, prompt: Text("Company Name"))
                    .padding()
                    .border(.gray, width: 1)
            }
            Spacer()
            NavigationLink(isActive: $goToHomepage) {
                HomepageView()
            } label: {
                Button {
//                    loginViewModel.joinCompany(companyCode: companyCode, name: employeeName)
                    //goToHomepage = true
                } label: {
                    Text("Join")
                        .frame(width: 300, height: 40)
                        .background(.blue)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
            }.alert(isPresented: $codeInvalid) {
                Alert(title: Text("Code Invalid"), message: Text("Your Company Code Is Wrong"), dismissButton: .cancel())
            }
        }.padding()
            .onChange(of: loginViewModel.isValid) { newValue in
                if newValue {
                    print("valid")
                    goToHomepage = true
                } else {
                    print("invalid code e")
                    codeInvalid = true
                }
            }
    }
}

struct JoinCompanyView_Previews: PreviewProvider {
    static var previews: some View {
        JoinCompanyView()
    }
}
