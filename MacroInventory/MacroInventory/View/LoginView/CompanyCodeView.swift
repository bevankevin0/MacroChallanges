//
//  CompanyCodeView.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import SwiftUI

struct CompanyCodeView: View {
    var companyCode: String
    @State var goToHomepage = false
    var body: some View {
        VStack(alignment:.center,spacing: 30) {
            Spacer()
            Text("Your Company Code")
            Text(companyCode)
                .multilineTextAlignment(.center)
                .frame(width: 190, height: 50)
                .border(.gray, width: 1)
            Text("You can share to your employee")
                .font(.caption)
                .foregroundColor(.gray)
            Spacer()
            NavigationLink(isActive: $goToHomepage) {
                HomepageView()
            } label: {
                Button {
                    goToHomepage = true
                } label: {
                    Text("Join")
                        .frame(width: 300, height: 40)
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
            }
        }
    }
}

struct CompanyCodeView_Previews: PreviewProvider {
    static var previews: some View {
        CompanyCodeView(companyCode: "er")
    }
}
