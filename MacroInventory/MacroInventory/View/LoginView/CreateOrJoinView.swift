import SwiftUI

struct CreateOrJoinView: View {
    @State var isCreateCompany = false
    @State var isJoinCompany = false
    var body: some View {
        VStack(alignment:.center) {
            Spacer()
            Text("Register")
                .font(.title)
            VStack(alignment: .center, spacing: 70) {
                Text("Become a member of Inc")
                    .font(.subheadline)
                    .foregroundColor(.gray)
                NavigationLink(isActive: $isCreateCompany) {
                    CreateCompanyView()
                } label: {
                    Button(action: {
                        isCreateCompany = true
                    }, label: {
                        Text("Create Company")
                            .frame(width: 300, height: 50)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    })
                }
                HStack {
                    VStack {
                        Divider()
                            .frame(height: 2)
                            .background(Color.gray)
                            .opacity(0.4)
                    }.padding()
                    Text("Or")
                    VStack {
                        Divider()
                            .frame(height: 2)
                            .background(Color.gray)
                            .opacity(0.4)
                    }.padding()
                }
                NavigationLink(isActive: $isJoinCompany) {
                    JoinCompanyView()
                } label: {
                    Button(action: {
                        isJoinCompany = true
                    }, label: {
                        Text("Join Company")
                            .frame(width: 300, height: 50)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    })
                }
                NavigationLink {
                    EmptyView()
                } label: {
                    EmptyView()
                }
                
                NavigationLink {
                    EmptyView()
                } label: {
                    EmptyView()
                }

            }
            Spacer()
        }
    }
}

struct CreateOrJoinView_Previews: PreviewProvider {
    static var previews: some View {
        CreateOrJoinView()
    }
}
