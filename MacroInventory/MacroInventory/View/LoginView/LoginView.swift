//
//  LoginView.swift
//  MacroInventory
//
//  Created by bevan christian on 12/10/21.
//

import SwiftUI


struct LoginView: View {
    @ObservedObject var loginViewModel = LoginViewModel()
    var body: some View {
        NavigationView {
            VStack(alignment: .center) {
                Spacer()
                Text("Register")
                    .font(.title)
                VStack(alignment: .center, spacing: 100) {
                    Text("Become a member of Inc")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                    NavigationLink(isActive: $loginViewModel.isSuccessNewUser) {
                        CreateOrJoinView()
                    } label: {
                        SignInWithApple()
                            .frame(width: 300, height: 50)
                            .onTapGesture {
                                loginViewModel.signInTapped()
                            }
                    }
                }
                Spacer()
                NavigationLink(isActive: $loginViewModel.isSuccessOldUser) {
                    HomepageView()
                } label: {
                    EmptyView()
                }

            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
