//
//  ImportCSVPickView.swift
//  MacroInventory
//
//  Created by Priscilla Vanny Amelia on 11/10/21.
//

import SwiftUI

struct ImportCSVPickView: View {
    @State private var filePath = ""
    @State private var showDocumentPicker = false
    
    var body: some View {
        VStack {
            Text(filePath).padding()
            
            Button("Import CSV") {
                showDocumentPicker = true
            }
        }
        .sheet(isPresented: $showDocumentPicker) {
            DocumentPicker(filePath: $filePath)
        }
    }
}

struct ImportCSVPickView_Previews: PreviewProvider {
    static var previews: some View {
        ImportCSVPickView()
    }
}
