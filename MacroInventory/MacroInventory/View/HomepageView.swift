//
//  Homepage.swift
//  MacroInventory
//
//  Created by Kezia Yovita Chandra on 04/10/21.


import SwiftUI

struct HomepageView: View {
    private var twoColumnGrid = [GridItem(.flexible()), GridItem(.flexible())]
    @State private var showingSheet = false
    @ObservedObject var viewModel = HomePageViewModel()
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                SearchBar(text: .constant(""))
                if viewModel.itemList.isEmpty {
                    VStack(spacing: 10) {
                        Image(systemName: "plus.circle.fill")
                            .resizable().frame(width: 30, height: 30)
                        Text("Upload data").font(.subheadline).bold()
                        Text("Import File CSV or Manually").font(.caption).multilineTextAlignment(.center)
                    }
                    .padding()
                    .frame(width: 165, height: 230)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10.0)
                            .stroke(Color.black, lineWidth: 1)
                    )
                } else {
                    ScrollView(showsIndicators: false) {
                        LazyVGrid(columns: twoColumnGrid, spacing: 20) {
                            Button(action: {
                                showingSheet.toggle()
                            }) { VStack(spacing: 10) {
                                Image(systemName: "plus.circle.fill")
                                    .resizable().frame(width: 30, height: 30)
                                Text("Upload data").font(.subheadline).bold()
                                Text("Import File CSV or Manually").font(.caption).multilineTextAlignment(.center)
                            }.foregroundColor(.black)
                                    .padding()
                                    .frame(width: 165, height: 230)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 10.0)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                            }.sheet(isPresented: $showingSheet) {
                                AddItemView()
                            }
                            ForEach(viewModel.itemList.indices, id: \.self)
                            { i in
                                ItemView(viewModel: viewModel, index: i)
                            }
                        }
                    }.padding()
                }
            }.navigationBarTitle("My Stocks", displayMode: .large)
                .onAppear(perform: {
                    viewModel.getItemData()
                })
        }
    }
}
struct ItemView: View {
    let viewModel: HomePageViewModel
    let index: Int
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            if let itemImage = viewModel.itemList[index].image {
                Image(uiImage: UIImage(data: itemImage) ?? UIImage())
                    .resizable()
                    .frame(width: 140, height: 120, alignment: .center)
            }
            Text(viewModel.itemList[index].itemName ?? "")
                .font(.body).bold()
            HStack {
                Image(systemName: "shippingbox")
                Text("50").font(.footnote).bold()
            }
        }
        .padding()
        .frame(width: 165, height: 230)
        .overlay(
            RoundedRectangle(cornerRadius: 10.0)
                .stroke(Color.black, lineWidth: 1)
        )
    }
}
struct Homepage_Previews: PreviewProvider {
    static var previews: some View {
        HomepageView()
    }
}
