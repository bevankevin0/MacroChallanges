//
//  HistoryView.swift
//  MacroInventory
//
//  Created by Fransiscus Verrol Yaurentius on 06/10/21.
//

import SwiftUI

struct HistoryView: View {
    @State var chosenView: Options = .stockIn
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: .constant(""))
                Picker("Options", selection: $chosenView) {
                    ForEach(Options.allCases, id: \.self) {
                        Text($0.rawValue)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                .padding(.horizontal, 10)
                Spacer()
                ChosenOptionView(selectedMenu: chosenView)
                Spacer()
            }
            .navigationBarTitle("History", displayMode: .large)
            .navigationBarItems(trailing:
                Button(action: {}) { Image(systemName: "slider.horizontal.3")
                    .resizable()
                    .foregroundColor(Color.black)
                    .frame(width: 30, height: 30, alignment: .center)
            }
            )
        }
    }
}

enum Options: String, CaseIterable {
    case stockIn = "Stock In"
    case stockOut = "Stock Out"
    case all = "All"
}

struct ChosenOptionView: View {
    
    var selectedMenu: Options
    
    var body: some View {
        VStack {
            Text("Thursday, 07 July 2021").font(.subheadline).bold()
            HStack(spacing: 30) {
                VStack(alignment: .center, spacing: 10) {
                    Text("150").font(.title3).bold()
                    Text("Stock In").font(.footnote).bold()
                }.foregroundColor(.green)
                Rectangle().frame(width: 1).foregroundColor(.gray)
                VStack(alignment: .center, spacing: 10) {
                    Text("150").font(.title3).bold()
                    Text("Stock Out").font(.footnote).bold()
                }.foregroundColor(.blue)
            }
        }.padding()
            .frame(width: 330, height: 120)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.gray, lineWidth: 1)
        )
        switch selectedMenu {
        case .stockIn:
            StockInView()
        case .stockOut:
            StockOutView()
        case .all:
            //StockOutView()
            StockAllView()
        }
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView()
    }
}
