//
//  DetailItemKeyboardView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 07/10/21.
//

import SwiftUI

struct DetailItemKeyboardView: View {
    @ObservedObject var sharedVm: SharedViewModel
    var viewController: KeyboardViewController!
//    @Binding var switchConfirmationPage: Int
    @Binding var showConfirmation: Bool
    @State var imagePresented = false
    var body: some View {
        VStack(spacing:10) {
            Spacer()
            List {
                ForEach(0...3,id:\.self) { _ in
                    VStack {
                        StockRowNoStepperComponentView()
                    }
                }
            }.listStyle(InsetListStyle())
            Button(action: {
                imagePresented = true
            }, label: {
                Text("Upload Bill")
                    .frame(width:150,height:35)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(8)
            })
            .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Confirmed") {
                            print("Pressed")
                            sharedVm.switchConfirmationPage = 0
                            viewController.removeConfirmation()
                            showConfirmation = false
                        }
                    }
                }
                .navigationTitle("Confirmation")
            Spacer()
        }.sheet(isPresented: $imagePresented) {
            print("dismiss")
        } content: {
            PickerViewGalery(didFinishPicking: { _ in
                imagePresented = false
            }, mediaItems: sharedVm)
        }.onDisappear {
            sharedVm.switchConfirmationPage = 0
        }
    }
}

//struct DetailItemKeyboardView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailItemKeyboardView(switchConfirmationPage: .constant(1), showConfirmation: .constant(true)).frame(height:510)
//    }
//}
