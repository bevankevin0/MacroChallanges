//
//  StockViewKeyboard.swift
//  KeyboardExtension
//
//  Created by bevan christian on 07/10/21.
//

import SwiftUI

struct StockViewKeyboard: View {
    @ObservedObject var sharedVm: SharedViewModel
    var viewController: KeyboardViewController!
    @State var searchText = ""
    @State var showConfirmation = false
    var body: some View {
        GeometryReader { _ in
            NavigationView {
                VStack {
                    List {
                        Group {
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color("LightGray"))
                                HStack {
                                    Image(systemName: "magnifyingglass")
                                    TextField("Search ..", text: $searchText)
                                }
                                .foregroundColor(.gray)
                                .padding(.leading, 13)
                            }
                            .frame(height: 20)
                            .cornerRadius(13)
                            .padding(.bottom, 5)
                            .onTapGesture {
                                print("firing")
                                sharedVm.switchAnotherKeyboard = 1
                                viewController.heightKeyboard = 560
                                viewController.heigthKeyboardCombing.send(510)
                            }
                        }.frame(height: 50)
                        ForEach(0...100, id: \.self) { _ in
                            VStack {
                                StockRowComponentView()
                            }
                        }
                    }.listStyle(InsetListStyle())
                }
                .navigationBarItems(trailing:
                                    NavigationLink(isActive: $showConfirmation, destination: {
                    DetailItemKeyboardView(sharedVm: sharedVm, viewController: viewController, showConfirmation: $showConfirmation)
                }, label: {
                    Button {
                        viewController.layoutingConfirmation {
                            sharedVm.switchAnotherKeyboard = 0
                            sharedVm.switchConfirmationPage = 1
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                                print("firing confirmation 2")
                                showConfirmation = true
                            }
                        }
                    } label: {
                        Text("Add")
                    }

                })
                .foregroundColor(.blue))
                .navigationBarTitleDisplayMode(.inline)
            }.ignoresSafeArea(.all)
        }
    }
}

//struct StockViewKeyboard_Previews: PreviewProvider {
//    static var previews: some View {
//        StockViewKeyboard(switchAnotherKeyboard:.constant(1), heightStockView: .constant(500), switchConfirmationPage: .constant(1)).frame(height:510)
//    }
//}
