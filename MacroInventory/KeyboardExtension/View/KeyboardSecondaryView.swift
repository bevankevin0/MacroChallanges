//
//  KeyboardSecondaryView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 07/10/21.
//

import SwiftUI

struct KeyboardSecondaryView: View {
    var viewController: KeyboardViewController!
    @State var switchKeyboard = 0
    @State var switchPage = 0
    @State var switchAnotherKeyboard = 0
    var body: some View {
        VStack(spacing: 0) {
            if switchPage == 0 {
                if switchKeyboard == 0 {
                    AlphabetView(switchKeyboard: $switchKeyboard, viewController: viewController)
                } else {
                    NumerikalKeyboard(switchKeyboard: $switchKeyboard, viewController: viewController)
                }
            }
    }
}
}

struct KeyboardSecondaryView_Previews: PreviewProvider {
    static var previews: some View {
        KeyboardSecondaryView()
    }
}
