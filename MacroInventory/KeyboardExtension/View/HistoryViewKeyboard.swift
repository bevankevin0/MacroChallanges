//
//  HistoryViewKeyboard.swift
//  KeyboardExtension
//
//  Created by bevan christian on 07/10/21.
//

import SwiftUI

struct HistoryViewKeyboard: View {
    var body: some View {
        VStack {
            List {
                ForEach(0...3,id:\.self) { _ in
                        VStack {
                            HStack {
                                Image("Nature")
                                    .resizable()
                                    .clipped()
                                    .scaledToFit()
                                    .frame(width: 60, height: 60, alignment: .center)
                                
                                VStack(alignment: .leading) {
                                    
                                    Text("Keripik Manis")
                                        .bold()
                                        .multilineTextAlignment(.leading)
                                    HStack {
                                        VStack(alignment:.leading) {
                                            Text("1 Pcs")
                                                .font(.caption)
                                            .bold()
                                            Text("By admin 1")
                                                .font(.caption)
                                        }
                                        Spacer()
                                        Text("Rp.10.000")
                                            .font(.caption)
                                    }
                                }
                                Spacer()
                            }
                        }
                }
            }.listStyle(InsetListStyle())
        }
    }
}

struct HistoryViewKeyboard_Previews: PreviewProvider {
    static var previews: some View {
        HistoryViewKeyboard()
    }
}
