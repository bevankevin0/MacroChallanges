//
//  SharedViewModel.swift
//  KeyboardExtension
//
//  Created by bevan christian on 09/10/21.
//

import Foundation
import SwiftUI

class SharedViewModel: ObservableObject {
    @Published var searchText = ""
    @Published var switchKeyboard = 0
    @Published var switchPage = 0
    @Published var switchAnotherKeyboard = 0
    @Published var switchConfirmationPage = 0
    @Published var itemsContentPicOrVid: UIImage?
    
    func append(item: UIImage) {
        itemsContentPicOrVid = item
        print("photo \(itemsContentPicOrVid)")
    }
}
