//
//  KeyboardView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 06/10/21.
//
// swiftlint:disable line_length
import SwiftUI

struct KeyboardView: View {
    @StateObject var sharedVm = SharedViewModel()
    var viewController: KeyboardViewController!
    var body: some View {
        GeometryReader { geo in
            ZStack {
                Color.themes.keyboardBackground
                VStack() {
                    ToolbarViewKeyboard(viewController: viewController, sharedVm: sharedVm)
                        .frame(height: sharedVm.switchAnotherKeyboard == 1 ? geo.size.height * 0.1 : geo.size.height * 0.2)
                    if sharedVm.switchPage == 0 {
                        if sharedVm.switchKeyboard == 0 {
                            AlphabetView(switchKeyboard: $sharedVm.switchKeyboard, viewController: viewController)
                                .frame(height: geo.size.height * 0.8)
                        } else {
                            NumerikalKeyboard(switchKeyboard: $sharedVm.switchKeyboard, viewController: viewController)
                                .frame(height: geo.size.height * 0.8)
                        }
                    } else if sharedVm.switchPage == 1 {
                        StockViewKeyboard(sharedVm: sharedVm, viewController: viewController)
                            .frame(height: sharedVm.switchAnotherKeyboard == 1 ? geo.size.height * 0.45 : geo.size.height * 0.85)
                            .offset(y: sharedVm.switchConfirmationPage == 0 ? -10 : -30)
                        if sharedVm.switchAnotherKeyboard == 1 {
                            VStack(spacing: 0) {
                                KeyboardSecondaryView(viewController: viewController)
                                    .frame(height: sharedVm.switchAnotherKeyboard == 1 ? geo.size.height * 0.45: geo.size.height * 0.8)
                            }
                        }
                    } else if sharedVm.switchPage == 2 {
                        HistoryViewKeyboard()
                            .frame(height: geo.size.height * 0.8)
                            .offset(y: -10)
                            .onAppear {
                                sharedVm.switchAnotherKeyboard = 0
                                viewController.removalSecondaryKeyboard.send(2)
                            }
                    } else {
                        EmptyView()
                    }
                    Spacer()
                }.ignoresSafeArea(.all)
            }
        }
    }
}

struct KeyboardView_Previews: PreviewProvider {
    static var previews: some View {
        KeyboardView().frame(height: 285).background(Color.red)
    }
}
