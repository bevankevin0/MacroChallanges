//
//  KeyboardViewController.swift
//  KeyboardExtension
//
//  Created by bevan christian on 05/10/21.
//
import SwiftUI
import UIKit
import Combine

class KeyboardViewController: UIInputViewController {
    var heigthKeyboardCombing = PassthroughSubject<Int, Never>()
    var removalSecondaryKeyboard = PassthroughSubject<Int, Never>()
    @IBOutlet var nextKeyboardButton: UIButton!
    var heightKeyboard = 280
    var subscription = Set<AnyCancellable>()
    // MARK: VIEW
    var heightViewConstraint: NSLayoutConstraint!
    
    // MARK: KEYBOARD
    var bottomConstraint: NSLayoutConstraint!
    var leftConstraint: NSLayoutConstraint!
    var rightConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!
    var topConstraint: NSLayoutConstraint!
    // MARK: SECONDARY KEYBOARD
    var heightSecondaryConstraint: NSLayoutConstraint!
    var topSecondaryConstraint: NSLayoutConstraint!
    
    var hosting: UIHostingController<KeyboardView>!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(Color.themes.keyboardBackground)
        layoutingMainKeyboard()
        layoutingSecondaryKeyboard()
        removeSecondaryKeyboard()
    }
    
 
    private func layoutingMainKeyboard() {
        hosting = UIHostingController(rootView: KeyboardView(viewController: self))
        heightViewConstraint = view.heightAnchor.constraint(equalToConstant: 285)
        bottomConstraint = hosting.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant:0)
        leftConstraint = hosting.view.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor)
        rightConstraint = hosting.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        heightConstraint = hosting.view.heightAnchor.constraint(equalToConstant: 280)
        topConstraint = hosting.view.topAnchor.constraint(equalTo: view.topAnchor)
        view.addSubview(hosting.view)
        addChild(hosting)
        hosting.didMove(toParent: self)
        hosting.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            heightViewConstraint,
            leftConstraint,
            bottomConstraint,
            rightConstraint,
            topConstraint
            //heightConstraint
        ])
    }
    func layoutingSecondaryKeyboard() {
        heigthKeyboardCombing.sink { [self] num in
            heightViewConstraint.isActive = false
            bottomConstraint.isActive = false
            leftConstraint.isActive = false
            rightConstraint.isActive = false
            heightConstraint.isActive = false
            topConstraint.isActive = false
            heightSecondaryConstraint = view.heightAnchor.constraint(equalToConstant: CGFloat(510))
            topSecondaryConstraint = hosting.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 120)
            NSLayoutConstraint.activate([
                heightSecondaryConstraint,
                topConstraint,
                leftConstraint,
                rightConstraint,
                bottomConstraint
//                heightConstraint
            ])
            hosting.updateViewConstraints()
            self.view.layoutSubviews()
        }.store(in: &subscription)
    }
    func removeSecondaryKeyboard() {
        print("remove secondary")
        removalSecondaryKeyboard.sink { [self] _ in
            print("remove secondary")
            guard let secondary = heightSecondaryConstraint else { return  }
            guard let top = topSecondaryConstraint else { return  }
            heightSecondaryConstraint.isActive = false
            topSecondaryConstraint.isActive = false
            NSLayoutConstraint.activate([
                heightViewConstraint,
                bottomConstraint
            ])
            hosting.updateViewConstraints()
            self.view.layoutSubviews()
        }.store(in: &subscription)
    }
    
    func layoutingConfirmation(completion: @escaping() -> Void) {
        print("firing confirmation")
        heightViewConstraint.isActive = false
        bottomConstraint.isActive = false
        leftConstraint.isActive = false
        rightConstraint.isActive = false
        heightConstraint.isActive = false
        topConstraint.isActive = false
        heightSecondaryConstraint = view.heightAnchor.constraint(equalToConstant: CGFloat(510))
        topSecondaryConstraint = hosting.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 120)
        NSLayoutConstraint.activate([
            heightSecondaryConstraint,
            topConstraint,
            leftConstraint,
            rightConstraint,
            bottomConstraint
        ])
        hosting.updateViewConstraints()
        self.view.layoutSubviews()
        completion()
    }
    
    
    func removeConfirmation() {
        bottomConstraint.isActive = false
        leftConstraint.isActive = false
        rightConstraint.isActive = false
        topConstraint.isActive = false
        NSLayoutConstraint.activate([
            heightViewConstraint,
            topConstraint,
            leftConstraint,
            rightConstraint,
            bottomConstraint
        ])
        hosting.updateViewConstraints()
        self.view.layoutSubviews()
    }
}
