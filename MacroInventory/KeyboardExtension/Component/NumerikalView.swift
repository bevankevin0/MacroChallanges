//
//  NumerikalKeyboard.swift
//  keyboardSwiftui
//
//  Created by bevan christian on 06/10/21.
//

import SwiftUI
import AudioToolbox

struct NumerikalKeyboard: View {
    @Binding var switchKeyboard: Int
    var viewController: KeyboardViewController!
    var alphabet = [["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
                    ["-", "/", ":", ";", "(", ")", "$", "&", "@", "\""],
                    [".",",","?","!","'"]]
    var tandaBaca = [["[", "]", "{", "}", "#", "%", "^", "*", "+", "="],
                    ["_", "\\", "|", "~", "<", ">", "€", "£", "¥", "•"]]
    @State var isTandaBaca = false
    var body: some View {
        ZStack {
            Color.themes.keyboardBackground
            GeometryReader { geo in
                VStack {
                    Spacer(minLength: 2)
                    HStack(spacing: 5.5) {
                        Spacer(minLength: 2)
                        ForEach(isTandaBaca ? tandaBaca [0] : alphabet[0], id: \.self) { huruf in
                            Text(huruf)
                                .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                .onTapGesture {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(huruf)
                                }
                        }
                        Spacer(minLength: 2)
                    }.padding(.bottom, 4)

                    HStack(spacing: 5.5) {
                        Spacer(minLength: 2)
                        ForEach(isTandaBaca ? tandaBaca [1] : alphabet[1], id: \.self) { huruf in
                            Text(huruf)
                                .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[1].count, height: 200, keyCapsVerticallCount: 5))
                                .onTapGesture {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(huruf)
                                }
                        }
                        Spacer(minLength: 2)
                    }
                    HStack(spacing: 5.5) {
                        Spacer(minLength: 2)
                        Text(isTandaBaca ? "123"  : "#+=")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                isTandaBaca.toggle()
                            }
                        Spacer(minLength: 2)
                        ForEach(alphabet[2], id: \.self) { huruf in
                            Text(huruf)
                                .modifier(NormalBottomAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                .onTapGesture {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(huruf)
                                }
                        }
                        Spacer(minLength: 2)
                        Text("⌫")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1155)
                                viewController.textDocumentProxy.deleteBackward()
                            }
                        Spacer(minLength: 2)
                    }.padding(.top, 4)
                    HStack(spacing: 5.5) {
                        Spacer(minLength: 2)
                        Text("ABC")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                switchKeyboard = 0
                            }
                        Text("🌐")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                viewController.advanceToNextInputMode()
                            }
                            Text("space")
                            .frame(width: (geo.size.width / 3 + 45), height: 200 / 5.5)
                                .font(.system(size: 16))
                                .padding(3)
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(color: Color.gray.opacity(0.5), radius: 3, x: 1, y: 1)
                                .onTapGesture {
                                    AudioServicesPlaySystemSound(1156)
                                    viewController.textDocumentProxy.insertText(" ")
                                }
                        Text("go")
                            .frame(width: (geo.size.width / 3 - 51), height: 200 / 5.5)
                            .font(.system(size: 18))
                            .padding(.leading, 0.2)
                            .padding(3)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(5)
                            .shadow(color: Color.gray.opacity(0.5), radius: 3, x: 1, y: 1)
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                viewController.textDocumentProxy.insertText("\n")
                            }
                        Spacer(minLength: 2)
                    }.padding(.top, 4)
                }.padding(.bottom, 20)
            }
        }
    }
}
//struct NumerikalKeyboard_Previews: PreviewProvider {
//    static var previews: some View {
//        NumerikalKeyboard()
//    }
//}
