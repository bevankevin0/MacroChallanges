//
//  StockRowNoStepperComponentView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 09/10/21.
//

import SwiftUI

struct StockRowNoStepperComponentView: View {
    var body: some View {
        VStack {
            HStack {
                Image("Nature")
                    .resizable()
                    .clipped()
                    .scaledToFit()
                    .frame(width: 90, height: 90, alignment: .center)
                
                VStack(alignment: .leading) {
                    Text("Keripik Manis")
                        .bold()
                        .multilineTextAlignment(.leading)
                    HStack {
                        VStack(alignment: .leading) {
                            Text("1 Pcs")
                                .font(.caption)
                                .bold()
                            Text("By admin 1")
                                .font(.caption)
                        }
                        Spacer()
                        Text("Rp.10.000")
                            .font(.caption)
                    }
                }
                Spacer()
            }
        }
    }
}

struct StockRowNoStepperComponentView_Previews: PreviewProvider {
    static var previews: some View {
        StockRowNoStepperComponentView()
    }
}
