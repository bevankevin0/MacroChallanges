//
//  ToolbarViewKeyboard.swift
//  KeyboardExtension
//
//  Created by bevan christian on 07/10/21.
//

import SwiftUI

struct ToolbarViewKeyboard: View {
    var viewController: KeyboardViewController!
    @ObservedObject var sharedVm: SharedViewModel
//    @Binding var switchPage: Int
//    @Binding var switchAnotherKeyboard: Int
    var body: some View {
            HStack {
                Button {
                    sharedVm.switchPage = 1
                } label: {
                    Text("Stock").bold()
                }
                Spacer()
                Button {
                    sharedVm.switchPage = 2
                } label: {
                    Text("History").bold()
                }
                Spacer()
                Button {
                    sharedVm.switchPage = 0
                    sharedVm.switchAnotherKeyboard = 0
                    viewController.removalSecondaryKeyboard.send(2)
                } label: {
                    Text("Keyboard")
                        .bold()
                }
            }
            .padding()
            .background(Color.gray)
            .foregroundColor(.white)
            .ignoresSafeArea(.all)
    }
}
