//
//  StockRowComponentView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 09/10/21.
//

import SwiftUI

struct StockRowComponentView: View {
    @State var buyStock = 0
    var body: some View {
        VStack {
            HStack {
                Image("Nature")
                    .resizable()
                    .clipped()
                    .cornerRadius(3)
                    .frame(width: 80, height: 80, alignment: .center)
                VStack(alignment: .leading, spacing: 10) {
                    Text("Keripik Manis")
                        .bold()
                        .multilineTextAlignment(.leading)
                    HStack(spacing: 0) {
                        VStack(alignment: .leading) {
                            Text("Rp.10000")
                                .font(.caption)
                                .bold()
                            Text("SKU 12")
                                .font(.caption)
                        }
                        Spacer()
                    }
                }
                VStack(alignment: .trailing) {
                    Text("QTY \(buyStock)")
                        .bold()
                    Stepper("") {
                        buyStock += 1
                    } onDecrement: {
                        if buyStock > 0 {
                            buyStock -= 1
                        }
                    }
                }
                Spacer()
            }
        }.foregroundColor(.black)
    }
}

struct StockRowComponentView_Previews: PreviewProvider {
    static var previews: some View {
        StockRowComponentView()
    }
}
