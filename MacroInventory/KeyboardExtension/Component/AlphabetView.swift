//
//  AlphabetView.swift
//  KeyboardExtension
//
//  Created by bevan christian on 06/10/21.
//

import Foundation
import SwiftUI
import AudioToolbox
// halo
struct AlphabetView: View {
    @Binding var switchKeyboard: Int
    var viewController: KeyboardViewController!
    var alphabet = [["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
                    ["a", "s", "d", "f", "g", "h", "j", "k", "l"],
                    ["z", "x", "c", "v", "b", "n", "m"]]
    var alphabetUppercase = [["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
                             ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
                             ["Z", "X", "C", "V", "B", "N", "M"]]
    @State var isUppercased = false
    @State var isLongPressing = false
    @State private var timer: Timer?
    var body: some View {
        ZStack {
            Color.themes.keyboardBackground
            GeometryReader { geo in
                VStack {
                    Spacer(minLength: 2)
                    HStack(spacing: 0.5) {
                        Spacer(minLength: 2)
                        ForEach(alphabet[0], id: \.self) { huruf in
                            Button {
                                DispatchQueue.global(qos: .userInteractive).async {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                                }
                            } label: {
                                ZStack {
                                    Rectangle()
                                        .modifier(NormalPaddingAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                    Text(isUppercased ? huruf.uppercased() : huruf )
                                        .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                        .foregroundColor(.black)
                                }
                            }
                            //                            Text(isUppercased ? huruf.uppercased() : huruf )
                            //                                .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            //                                .onTapGesture {
                            //                                    DispatchQueue.global(qos: .userInteractive).async {
                            //                                        UIDevice.current.playInputClick()
                            //                                        viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                            //                                    }
                            //                                }
                        }
                        Spacer(minLength: 2)
                    }.padding(.bottom, 1)
                    HStack(spacing: 0.5) {
                        ForEach(alphabet[1], id: \.self) { huruf in
                            // text vs button
                            Button {
                                DispatchQueue.global(qos: .userInteractive).async {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                                }
                            } label: {
                                ZStack {
                                    Rectangle()
                                        .modifier(NormalPaddingAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                    Text(isUppercased ? huruf.uppercased() : huruf )
                                        .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                        .foregroundColor(.black)
                                }
                               
                            }
                            //                            Text(isUppercased ? huruf.uppercased() : huruf )
                            //                                .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            //                                .onTapGesture {
                            //                                    UIDevice.current.playInputClick()
                            //                                    viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                            //                                }
                        }
                    }
                    HStack(spacing: 0.5) {
                        Spacer(minLength: 2)
                        Text("⇧")
                            .bold()
                            .modifier(UppercasedAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .background(isUppercased == true ? Color.blue : Color.themes.keyboardFrontHelper)
                            .cornerRadius(5)
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                isUppercased.toggle()
                            }
                        Spacer(minLength: 2)
                        ForEach(alphabet[2], id: \.self) { huruf in
                            Button {
                                DispatchQueue.global(qos: .userInteractive).async {
                                    UIDevice.current.playInputClick()
                                    viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                                }
                            } label: {
                                ZStack {
                                    Rectangle()
                                        .modifier(NormalPaddingAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                    Text(isUppercased ? huruf.uppercased() : huruf )
                                        .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                                        .foregroundColor(.black)
                                }
                            }
                            //                            Text(isUppercased ? huruf.uppercased() : huruf )
                            //                                .modifier(NormalAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            //                                .onTapGesture {
                            //                                    UIDevice.current.playInputClick()
                            //                                    viewController.textDocumentProxy.insertText(isUppercased ? huruf.uppercased() : huruf)
                            //                                }
                        }
                        Spacer(minLength: 2)
                        Button {
                            print("tap")
                            if(self.isLongPressing) {
                                //this tap was caused by the end of a longpress gesture, so stop our fastforwarding
                                self.isLongPressing.toggle()
                                self.timer?.invalidate()
                            } else {
                                AudioServicesPlaySystemSound(1155)
                                viewController.textDocumentProxy.deleteBackward()
                            }
                        } label: {
                            Text("⌫")
                                .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                        }.simultaneousGesture(LongPressGesture(minimumDuration: 0.2).onEnded { _ in
                            print("long press")
                            self.isLongPressing = true
                            //or fastforward has started to start the timer
                            self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
                                AudioServicesPlaySystemSound(1155)
                                viewController.textDocumentProxy.deleteBackward()
                            })
                        })
                        Spacer(minLength: 2)
                    }.padding(.top, 1)
                    HStack(spacing: 5.5) {
                        Text("123")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                switchKeyboard = 1
                            }
                        Text("🌐")
                            .modifier(MediumAlphabetKeyboard(width: geo.size.width, keyCapsHorizontalCount: alphabet[0].count, height: 200, keyCapsVerticallCount: 5))
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                viewController.advanceToNextInputMode()
                            }
                        Text("space")
                            .frame(width: (geo.size.width / 3 + 45), height: 200 / 5.5)
                            .font(.system(size: 16))
                            .padding(3)
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(color: Color.gray.opacity(0.5), radius: 3, x: 1, y: 1)
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                viewController.textDocumentProxy.insertText(" ")
                            }
                        Text("go")
                            .frame(width: (geo.size.width / 3 - 51), height: 200 / 5.5)
                            .font(.system(size: 18))
                            .padding(.leading, 0.2)
                            .padding(3)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(5)
                            .shadow(color: Color.gray.opacity(0.5), radius: 3, x: 1, y: 1)
                            .onTapGesture {
                                AudioServicesPlaySystemSound(1156)
                                viewController.textDocumentProxy.insertText("\n")
                            }
                    }.padding(.top, 1)
                }.padding(.bottom, 20)
            }
        }
    }
}

extension UIInputView: UIInputViewAudioFeedback {
    public var enableInputClicksWhenVisible: Bool {
        return true
    }
}

struct NormalAlphabetKeyboard: ViewModifier {
    var width: CGFloat
    var keyCapsHorizontalCount: Int
    var height: CGFloat
    var keyCapsVerticallCount: Int
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
                .frame(width: (width / CGFloat(keyCapsHorizontalCount) - 13.2), height: height / (CGFloat(keyCapsVerticallCount) + 0.5), alignment: .center)
                .multilineTextAlignment(.center)
                .font(.system(size: 25))
                .padding(3.5)
                .background(Color.white)
                .cornerRadius(5)
                .shadow(color: Color.gray.opacity(0.5),radius: 3,x: 1,y: 1)
        }
    }
}

struct NormalPaddingAlphabetKeyboard: ViewModifier {
    var width: CGFloat
    var keyCapsHorizontalCount: Int
    var height: CGFloat
    var keyCapsVerticallCount: Int
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
                .frame(width: (width / CGFloat(keyCapsHorizontalCount) - 13.2), height: height / (CGFloat(keyCapsVerticallCount) + 0.5), alignment: .center)
                .font(.system(size: 25))
                .padding(6)
                .cornerRadius(5)
                .layoutPriority(0)
                .opacity(0)
                
        }
    }
}

struct NormalBottomAlphabetKeyboard: ViewModifier {
    var width: CGFloat
    var keyCapsHorizontalCount: Int
    var height: CGFloat
    var keyCapsVerticallCount: Int
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
                .frame(width: (width / CGFloat(keyCapsHorizontalCount)), height: height / (CGFloat(keyCapsVerticallCount) + 0.5), alignment: .center)
                .multilineTextAlignment(.center)
                .font(.system(size: 25))
                .padding(3.5)
                .background(Color.white)
                .cornerRadius(5)
                .shadow(color: Color.gray.opacity(0.5),radius: 3,x: 1,y: 1)
        }
    }
}
struct MediumAlphabetKeyboard: ViewModifier {
    // @Environment(\.colorScheme) var colorScheme
    var width: CGFloat
    var keyCapsHorizontalCount: Int
    var height: CGFloat
    var keyCapsVerticallCount: Int
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
                .frame(width: (width / CGFloat(keyCapsHorizontalCount) - 13) + 10, height: height / (CGFloat(keyCapsVerticallCount) + 0.5))
                .font(.system(size: 17))
                .padding(2.9)
                .background(Color.themes.keyboardFrontHelper)
                .cornerRadius(5)
                .shadow(color: Color.gray.opacity(0.5),radius: 3,x: 1,y: 1)
        }
    }
}

struct UppercasedAlphabetKeyboard: ViewModifier {
    // @Environment(\.colorScheme) var colorScheme
    var width: CGFloat
    var keyCapsHorizontalCount: Int
    var height: CGFloat
    var keyCapsVerticallCount: Int
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
                .frame(width: (width / CGFloat(keyCapsHorizontalCount) - 13) + 10, height: height / (CGFloat(keyCapsVerticallCount) + 0.5))
                .font(.system(size: 17))
                .padding(2.9)
                .shadow(color: Color.gray.opacity(0.5),radius: 3,x: 1,y: 1)
        }
    }
}
