//
//  Constant.swift
//  MacroInventory
//
//  Created by bevan christian on 13/10/21.
//

import Foundation

enum Constant {
    case userLoginProfile
    var stringValue: String {
        switch self {
        case .userLoginProfile:
            return "loginProfile"
        }
    }
}


enum CoreDataError: Error {
    case coredataerror
}

