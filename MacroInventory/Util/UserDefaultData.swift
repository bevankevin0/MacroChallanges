//
//  UserDefaultData.swift
//  MacroInventory
//
//  Created by bevan christian on 14/10/21.
//

import Foundation


class UserDefaultData {
    static let shared = UserDefaultData()
    
    func getUserData() -> UserModel? {
        // sudah pernah login dilempar ke homepage langsung dan di get team codenya apa?
        guard let appleUserData = UserDefaults.standard.data(forKey: Constant.userLoginProfile.stringValue),
              let appleUser = try? JSONDecoder().decode(UserModel.self, from: appleUserData)
        else { return nil }
        
        return appleUser
    }
}
